#!/usr/bin/env python
'''
Created on 11/09/2014

@author: dlawrence
'''

from Bio import SeqIO
from argparse import ArgumentParser
from collections import Counter
import re

import numpy as np
import pandas as pd
from sacgf_library.barcodes import get_library_barcodes_set
from sacgf_library.graphing import write_graph, CounterDistributionGraph
from sacgf_library.utils import name_from_file_name, get_open_func, sequence_file_format


def handle_args():
    # Args
    parser = ArgumentParser(description='Show barcodes from fast[qa] file')
    parser.add_argument("--library", action='append', help='Known library sequences to filter to')
    parser.add_argument("--barcode", required=True)
    parser.add_argument('sequence_file',   metavar='sequence_file', help='fasta or fastq file')
    return parser.parse_args()

def sequence_to_regex(sequence):
    conversion = {'N' : '[GATC]',
                  'S' : '[GC]'}
    
    regex = ''
    for nt in sequence:
        regex += conversion.get(nt, nt)
    return regex
            
def plot_graph(barcodes, title, name, hide_once_below=None):
    graph = CounterDistributionGraph(barcodes, hide_once_below)
    graph.title = title
    graph.x_label = "tag counts"
    graph.y_label = "Number of tags with count"

    graph_image = "%s.tag_count_distribution" % name
    if hide_once_below is not None:
        graph_image = "%s.no_outliers_below_%d" %  (graph_image, hide_once_below)

    graph.save("%s.png" % graph_image)


def main(sequence_file, barcode, libraries):
    print "barcode=%s" % barcode
    
    open_func = get_open_func(sequence_file)
    sequence_format = sequence_file_format(sequence_file)
    record_iterator = SeqIO.parse(open_func(sequence_file, "rb"), sequence_format)

#    pattern = "%s(%s)%s" % (BEFORE, sequence_to_regex(BARCODE), AFTER)
    pattern = sequence_to_regex(barcode)
    regex = re.compile(pattern)

    barcodes = Counter()

    name = name_from_file_name(sequence_file)
    barcodes_file = "%s.barcodes.txt" % name
    out_barcodes = open(barcodes_file, "w")

    no_barcodes_file = "%s.no_barcodes.txt" % name
    out_no_barcodes = open(no_barcodes_file, "w")

    if libraries:
        library_barcodes_set = get_library_barcodes_set(libraries)
        barcodes_not_library_file = "%s.barcodes_not_library.txt" % name
        out_barcodes_not_library = open(barcodes_not_library_file, "w")
    else:
        library_barcodes_set = None
    
    matches = 0
    matches_in_library = 0
    
    for i, record in enumerate(record_iterator):
        sequence = str(record.seq)
        m = regex.match(sequence)
        
        if m:
            matches += 1
            if not library_barcodes_set or sequence in library_barcodes_set:
                matches_in_library += 1
                barcodes[sequence] += 1
                out_file = out_barcodes
            else:
                out_file = out_barcodes_not_library
        else:
            out_file = out_no_barcodes            

        out_file.write(sequence + "\n")

    if "_R1_" in name:
        title = "R1" 
    elif "_R2_" in name:
        title = "R2"

    plot_graph(barcodes, title, name)
    plot_graph(barcodes, title, name, 1)

    top_10_sum = sum([s[1] for s in barcodes.most_common(10)])

    stats_csv = "%s.tag_count_stats.csv" % name
    series = pd.Series(index=["reads", "matches", "match perc", "uniq barcodes", "top 10 sum"])
    series['reads'] = i    
    series['matches'] = matches
    series['match perc'] = 100.0 * matches / float(i) 
    series['matches_in_library'] = matches_in_library
    series['matches_in_library perc'] = 100.0 * matches_in_library / float(i) 
    series['uniq barcodes'] = len(barcodes)
    series['top 10 sum'] = top_10_sum
    pd.DataFrame(series).T.to_csv(stats_csv, index=False)

    array = np.sort(barcodes.values())[::-1]
    perc = 100.0 * array / matches
    graph_image = "%s_tags_percent.png" % name
    write_graph(graph_image, np.arange(len(perc)), perc, title=title, x_label='Individual tags ranked by counts', y_label='% of matched reads') 
    
#    print barcodes.most_common(10) 


if __name__ == '__main__':
    args = handle_args()
    
    main(args.sequence_file, args.barcode, args.library)