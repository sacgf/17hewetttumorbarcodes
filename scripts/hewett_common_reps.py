#!/usr/bin/env python
'''
Created on 12/04/2016

@author: dlawrence
'''

import glob
import os
import re

import pandas as pd
from sacgf_library.barcodes import get_library_barcodes_set
from sacgf_library.utils import file_to_array, array_to_file, mk_path


WRITE_COMMON_BARCODES = True

COMBINED_LIBRARIES_R1 = "/data/sacgf/medicine/data/analysis/Hewett_Miseq_Tag_Counts/combined_libraries/libraries_R1_min_2.txt"
COMBINED_LIBRARIES_R2 = "/data/sacgf/medicine/data/analysis/Hewett_Miseq_Tag_Counts/combined_libraries/libraries_R2_min_2.txt"

REP_1_DIR = "/data/sacgf/medicine/data/analysis/150624DuncanHewett_MiSeq/All_Barcodes_No_Filtered"
REP_2_DIR = "/data/sacgf/medicine/data/analysis/160311DuncanHewett_MiSeq/All_Barcodes_No_Filtered"

REP_1_OUT_DIR = "150624_common"
REP_2_OUT_DIR = "160311_common"

REP_1_PATTERN = r"S(\d+)_S\d+_L001_(R[12])_001.trimmed.fastq.barcodes.txt"
REP_2_PATTERN = r"T-(\d+)_S\d+_L001_(R[12])_001.trimmed.fastq.barcodes.txt"

def get_directory_barcodes_set(directory, rep_pattern):
    files = {}
    for barcode in glob.glob1(directory, "*.fastq.barcodes.txt"):
        m = re.match(rep_pattern, barcode)
        if not m:
            msg = "'%s' didn't match pattern '%s'" % (barcode, rep_pattern)
            raise ValueError(msg)
        
        sample = m.group(1)
        read_end = m.group(2)
        
        sample_id = "S%s_%s" % (sample, read_end)
        files[sample_id] = os.path.join(directory, barcode)
    return files

def get_matched_reps(rep_1_files, rep_2_files):
    diff = set(rep_1_files.keys()) ^ set(rep_2_files.keys())
    if diff:
        msg = "rep1 and rep2 files not the same! Difference: %s" % diff
        raise ValueError(msg)
    
    matched_reps = {}
    for (base_name, rep_1_file_name) in rep_1_files.iteritems():
        rep_2_file_name =  rep_2_files[base_name]
        matched_reps[base_name] = (rep_1_file_name, rep_2_file_name)
        
    return matched_reps
    

def main():
    library_barcodes_by_end = {"R1" : get_library_barcodes_set([COMBINED_LIBRARIES_R1]),
                               "R2" : get_library_barcodes_set([COMBINED_LIBRARIES_R2]),}
    
    # Go through 2 directories and match up the samples
    rep_1_files = get_directory_barcodes_set(REP_1_DIR, REP_1_PATTERN)
    rep_2_files = get_directory_barcodes_set(REP_2_DIR, REP_2_PATTERN)

    matched_reps = get_matched_reps(rep_1_files, rep_2_files) 
    
    rep_stats = {}
    for sample_name, (rep_1_file_name, rep_2_file_name) in matched_reps.iteritems():
        #print "%s = (%s, %s)" % (sample_name, rep_1_file_name, rep_2_file_name)

        rep_1_barcodes = file_to_array(rep_1_file_name)
        rep_2_barcodes = file_to_array(rep_2_file_name)

        common_barcodes = set(rep_1_barcodes) & set(rep_2_barcodes)
        
        rep_1_common = [b for b in rep_1_barcodes if b in common_barcodes]
        rep_2_common = [b for b in rep_2_barcodes if b in common_barcodes]

        read_end = sample_name.split("_")[-1]
        library_barcodes_set = library_barcodes_by_end[read_end]
        rep_1_common_in_libraries = [b for b in rep_1_common if b in library_barcodes_set]
        rep_2_common_in_libraries = [b for b in rep_2_common if b in library_barcodes_set]
        
        rep_data = {"150624_count" : len(rep_1_barcodes),
                    "160311_count" : len(rep_2_barcodes),
                    "unique_common" : len(common_barcodes),
                    "150624_common" : len(rep_1_common),
                    "160311_common" : len(rep_2_common),
                    "150624_common_in_lib" : len(rep_1_common_in_libraries),
                    "160311_common_in_lib" : len(rep_2_common_in_libraries),}
        rep_stats[sample_name] = rep_data

        if WRITE_COMMON_BARCODES:
            mk_path(REP_1_OUT_DIR)
            mk_path(REP_2_OUT_DIR)
    
            rep_1_common_file_name = os.path.join(REP_1_OUT_DIR, "%s_common.txt" % sample_name)
            print "Writing %s" % rep_1_common_file_name
            array_to_file(rep_1_common_file_name, rep_1_common)
    
            rep_2_common_file_name = os.path.join(REP_2_OUT_DIR, "%s_common.txt" % sample_name)
            print "Writing %s" % rep_2_common_file_name
            array_to_file(rep_2_common_file_name, rep_2_common)

            rep_1_common_libraries_file_name = os.path.join(REP_1_OUT_DIR, "%s_common_in_libraries.txt" % sample_name)
            print "Writing %s" % rep_1_common_libraries_file_name
            array_to_file(rep_1_common_libraries_file_name, rep_1_common_in_libraries)
    
            rep_2_common_libraries_file_name = os.path.join(REP_2_OUT_DIR, "%s_common_in_libraries.txt" % sample_name)
            print "Writing %s" % rep_2_common_libraries_file_name
            array_to_file(rep_2_common_libraries_file_name, rep_2_common_in_libraries)


        
    df = pd.DataFrame.from_dict(rep_stats, orient='index')
    two_decimal_places = lambda f : "%.2f" % f

    common_perc_160311 = 100.0 * df["160311_common"] / df["160311_count"]
    common_perc_150624 = 100.0 * df["150624_common"] / df["150624_count"]
    df["160311_common_perc"] = common_perc_160311.apply(two_decimal_places) 
    df["150624_common_perc"] = common_perc_150624.apply(two_decimal_places) 
    
    common_in_lib_perc_160311 = 100.0 * df["160311_common_in_lib"] / df["160311_common"]
    common_in_lib_perc_150624 = 100.0 * df["150624_common_in_lib"] / df["150624_common"]
    df["160311_common_in_lib_perc"] = common_in_lib_perc_160311.apply(two_decimal_places) 
    df["150624_common_in_lib_perc"] = common_in_lib_perc_150624.apply(two_decimal_places) 
    
    # Get order of cols right
    COL_ORDER = ["160311_count", "160311_common", "160311_common_perc", "160311_common_in_lib", "160311_common_in_lib_perc",
                 "150624_count", "150624_common", "150624_common_perc", "150624_common_in_lib", "150624_common_in_lib_perc",
                 "unique_common"]
    df = df[COL_ORDER]
    df.to_csv("150624_160311_common_rep_stats.csv")


if __name__ == '__main__':
    main()