#!/usr/bin/env python
'''
Created on 15/04/2016

@author: dlawrence
'''

import glob
import os
import re

import pandas as pd
from sacgf_library.utils import sorted_nicely


#  individual barcode sequence down the side and the tumour "S" number across the top
ANALYSIS_DIR = "/data/sacgf/medicine/data/analysis"
DIR_141023 = os.path.join(ANALYSIS_DIR, '141023DuncanHewett_MiSeq/new_filter_2_of_4')
DIR_150624 = os.path.join(ANALYSIS_DIR, '150624DuncanHewett_MiSeq/common_reps_and_filtered_to_2_of_4_libraries')
DIR_160311 = os.path.join(ANALYSIS_DIR, '160311DuncanHewett_MiSeq/common_reps_and_filtered_to_2_of_4_libraries')

BARCODE_DIRS_EXTENSIONS_AND_PATTERNS = [("141023_in_2_of_4_libraries", DIR_141023, '_%(read_end)s_001.trimmed.fastq.barcodes.txt', '(\d+)_S\d+_L001_R[12]_001.trimmed.fastq.barcodes.txt'),
                                        ("150624_common_reps_in_2_of_4_libraries", DIR_150624, '_%(read_end)s_common_in_libraries.txt', '(S\d+)_R[12]_common_in_libraries.txt'),
                                        ("160311_common_reps_in_2_of_4_libraries", DIR_160311, '_%(read_end)s_common_in_libraries.txt', '(S\d+)_R[12]_common_in_libraries.txt'),
]


def common_barcodes_for_run(run_name, run_dir, extension_pattern, pattern):
    for read_end in ["R1", "R2"]:
        files_pattern = extension_pattern % {"read_end" : read_end}
        files = glob.glob1(run_dir, "*%s" % files_pattern)
    
        counts_for_sample = {}
        for barcode_file_name in files:
            m = re.match(pattern, barcode_file_name)
            if not m:
                msg = "Couldn't find '%s' in '%s'" % (pattern, barcode_file_name)
                raise ValueError(msg)
            sample_name = m.group(1)
            file_name = os.path.join(run_dir, barcode_file_name)
            print "Loading '%s'" % file_name
            df = pd.read_csv(file_name, header=None, index_col=False)
            barcode_series = df[0]
            counts_for_sample[sample_name] = barcode_series.value_counts()


        joined_df = pd.DataFrame(counts_for_sample, dtype=int)
        columns = sorted_nicely(joined_df.columns)
        joined_df = joined_df[columns]
        joined_df["total"] = joined_df.sum(axis=1)
        joined_df = joined_df.sort_values("total", ascending=False)
        print "Run_name = %s" % run_name
        joined_file_name = "%s_%s_barcodes_per_sample.csv" % (run_name, read_end)
        joined_df.to_csv(joined_file_name)


def main():
    for (run_name, run_dir, extension_pattern, pattern) in BARCODE_DIRS_EXTENSIONS_AND_PATTERNS:
        common_barcodes_for_run(run_name, run_dir, extension_pattern, pattern)

if __name__ == '__main__':
    main()