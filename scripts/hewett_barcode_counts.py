#!/usr/bin/env python
'''
Created on 08/12/2014

@author: dlawrence
'''
import glob
import os

import numpy as np
import pandas as pd
from sacgf_library.barcodes import get_library_barcodes_set
from sacgf_library.utils import name_from_file_name, sorted_nicely


# This should work for all entries below
def get_run_name(tag_count_dir):
    return tag_count_dir.split('/')[-2]


TAG_COUNT_DIRS = ["/data/sacgf/medicine/data/analysis/141023DuncanHewett_MiSeq/All_Barcodes_No_Filtered",
                  "/data/sacgf/medicine/data/analysis/150624DuncanHewett_MiSeq/All_Barcodes_No_Filtered",
                  "/data/sacgf/medicine/data/analysis/160121DuncanHewett_NextSeq/All_Barcodes_No_Filtered",
                  "/data/sacgf/medicine/data/analysis/160311DuncanHewett_MiSeq/All_Barcodes_No_Filtered",]

LIBRARY_DIR = "/data/sacgf/medicine/data/analysis/140902DuncanHewett_MiSeq"
LIBRARIES = {"R1" : [os.path.join(LIBRARY_DIR, "Less_S1_L001_R1_001.trimmed.fastq.barcodes.txt"),
                     os.path.join(LIBRARY_DIR, "More_S2_L001_R1_001.trimmed.fastq.barcodes.txt")],
             "R2" : [os.path.join(LIBRARY_DIR, "Less_S1_L001_R2_001.trimmed.fastq.barcodes.txt"),
                     os.path.join(LIBRARY_DIR, "More_S2_L001_R2_001.trimmed.fastq.barcodes.txt")],}

USE_SAMPLE_CSV = False

def get_barcode_counts(tag_count_dir, read_end, sample_df):
    files = glob.glob1(tag_count_dir, "*%s*.fastq.barcodes.txt" % read_end)

    data = {}
    for barcode in sorted_nicely(files):
        file_name = os.path.join(tag_count_dir, barcode)
        name = name_from_file_name(file_name)
        name = "S%s" % name.split('_')[0]
        print "name = %s" % name
        if sample_df:
            name = sample_df.ix[name]['Sample']
            print "name = %s" % name
        
        #print "file_name = %s" % file_name
        #series = pd.Series.from_csv(file_name, header=None, index_col=None)

        numpy_array = np.loadtxt(file_name, dtype='S')
        series = pd.Series(numpy_array)
        data[name] = series.value_counts()
        
    df = pd.DataFrame(data)
    df["total"] = df.sum(axis=1)
    if sample_df:
        df = df[['total'] + list(sample_df['Sample'])]
    return df.sort("total", ascending=False)


def main():
    if USE_SAMPLE_CSV:
        samples_filename = os.path.join(os.path.dirname(__file__), "data", "samples.csv")
        samples_df = pd.DataFrame.from_csv(samples_filename)
    else:
        samples_df = None
    
    print "Reading Library Barcodes"
    library_barcodes = {}
    for read_end in ["R1", "R2"]:
        libraries = LIBRARIES[read_end]
        library_barcodes[read_end] = get_library_barcodes_set(libraries)
    
    print "Processing Tag Count dirs"
    for tag_count_dir in TAG_COUNT_DIRS:
        run_name = get_run_name(tag_count_dir)
        
        for read_end in ["R1", "R2"]:
            print "Run: %s, read_end: %s" % (run_name, read_end)
            library_barcodes_set = library_barcodes[read_end]
            df = get_barcode_counts(tag_count_dir, read_end, samples_df)
            csv_file_name = "%s_%s_barcode_counts.csv" % (run_name, read_end)
            df.to_csv(csv_file_name)
            
            if library_barcodes_set:
                index = df.index.isin(library_barcodes_set)
                filtered_df = df[index]
                filtered_df.to_csv("library_filtered_" + csv_file_name)            


if __name__ == '__main__':
    main()