#!/usr/bin/env python
'''
Created on 11/04/2016

@author: dlawrence
'''

from collections import defaultdict, Counter
import os

import pandas as pd
from sacgf_library.utils import name_from_file_name, file_to_array, \
    array_to_file


MIN_SAMPLES_TO_WRITE_COMMON_BARCODES = 2
MIN_SAMPLES_TO_WRITE_CSV = 3

LENTIVIRAL_LIB_1_DIR = "/data/sacgf/medicine/data/analysis/140902DuncanHewett_MiSeq"
LENTIVIRAL_LIB_2_DIR = "/data/sacgf/medicine/data/analysis/160121DuncanHewett_NextSeq/All_Barcodes_No_Filtered"


LIBRARY_PATTERNS = [os.path.join(LENTIVIRAL_LIB_1_DIR, "More_S2_L001_%(read)s_001.trimmed.fastq.barcodes.txt"),
                    os.path.join(LENTIVIRAL_LIB_2_DIR, "PCR1_S1_%(read)s_001.trimmed.fastq.barcodes.txt"),
                    os.path.join(LENTIVIRAL_LIB_2_DIR, "PCR2_S2_%(read)s_001.trimmed.fastq.barcodes.txt"),
                    os.path.join(LENTIVIRAL_LIB_2_DIR, "PCR3_S3_%(read)s_001.trimmed.fastq.barcodes.txt"),]

def compare_libraries(csv_file_name, libraries):
    print "Reading barcodes"

    all_barcodes = set()
    library_sets = {}
    for l in libraries:
        parts = name_from_file_name(l).split("_")
        name = "_".join(parts[:2])
        barcodes_set = set(file_to_array(l))
        all_barcodes.update(barcodes_set)
        library_sets[name] = barcodes_set
    
    print "Counting barcodes"
    barcode_counts = Counter() 
    barcodes_by_total = defaultdict(dict)
    common_barcodes = []
    
    for bc in all_barcodes:
        library_counts = {}
        total = 0
        for (name, bc_set) in library_sets.iteritems():
            count = int(bc in bc_set)
            library_counts[name] = count
            total += count
             
        barcode_counts[total] += 1
        if total >= MIN_SAMPLES_TO_WRITE_CSV:
            barcodes_by_total[total][bc] = library_counts

        if total >= MIN_SAMPLES_TO_WRITE_COMMON_BARCODES:
            common_barcodes.append(bc)

    common_file_name = "%s_min_%d.txt" % (csv_file_name, MIN_SAMPLES_TO_WRITE_COMMON_BARCODES)
    array_to_file(common_file_name, common_barcodes)

    df = pd.DataFrame.from_dict(barcode_counts, orient='index')
    df.to_csv("%s_counts.csv" % csv_file_name)

    for (total, bc_counts) in barcodes_by_total.iteritems():
        df = pd.DataFrame.from_dict(bc_counts, orient='index')
        df.to_csv("%s_in_%d_samples.csv" % (csv_file_name, total))
    

def main():
    for read in ["R1", "R2"]:
        libraries = []
        for lib_pattern in LIBRARY_PATTERNS:
            libraries.append(lib_pattern % {"read" : read})
        
        csv_file_name = "libraries_%s" % read
        compare_libraries(csv_file_name, libraries)
        

if __name__ == '__main__':
    main()
