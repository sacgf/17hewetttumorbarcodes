from sacgf_library.utils import file_to_array


def get_library_barcodes_set(libraries): 
    library_barcodes_set = set()
    if libraries:
        for bc_l in libraries:
            bcs = file_to_array(bc_l)
            library_barcodes_set.update(bcs)
    return library_barcodes_set
