import gzip
import os
import re


def name_from_file_name(file_name):
    '''Gets file name with removing extension and directory'''
    return os.path.splitext(os.path.basename(file_name))[0]

def sorted_nicely(l): 
    """ Sort the given iterable in the way that humans expect.
        From http://www.codinghorror.com/blog/2007/12/sorting-for-humans-natural-sort-order.html """ 
    convert = lambda text: int(text) if text.isdigit() else text 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)

def array_to_file(file_name, array):
    mk_path_for_file(file_name)
    with open(file_name, "w") as f:
        for line in array:
            f.write(line + "\n")

def file_to_array(file_name, comment=None):
    array = []
    with open(file_name) as f:
        for line in f:
            if comment and line.startswith(comment):
                continue
            array.append(line.rstrip())
    return array

def mk_path(path):
    if path and not os.path.exists(path):
        os.makedirs(path)

def mk_path_for_file(f):
    mk_path(os.path.dirname(f))


def get_open_func(file_name):
    if file_name.endswith(".gz"):
        return gzip.open
    else:
        return open

def remove_gz_if_exists(filename):
    if filename.endswith(".gz"):
        filename = os.path.splitext(filename)[0] # remove .gz
    return filename

def sequence_file_format(filename):
    filename = remove_gz_if_exists(filename)
        
    if [f for f in [".fa", ".fasta"] if filename.endswith(f)]:
        return "fasta"
    elif [f for f in [".fq", ".fastq", ".txt"] if filename.endswith(f)]:
        return "fastq"
    raise Exception("Unknown sequence file type of %s" % (filename, ))

