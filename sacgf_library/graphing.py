import abc
import logging
from matplotlib import pyplot
import numpy as np
import matplotlib
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.cm import ScalarMappable
from matplotlib.figure import Figure
from matplotlib.patches import Rectangle

from sacgf_library import utils


matplotlib.use('agg', warn=False)

logger = logging.getLogger(__name__)

def write_graph(graph_image, largs, profile, **kwargs):
    logger.info("write_graph: %s", graph_image)
    
    title = kwargs.get("title")
    x_label = kwargs.get("x_label")
    y_label = kwargs.get("y_label")
    extra_func = kwargs.get("extra_func")
    legend_props = kwargs.get("legend_props", {})

    fig = pyplot.figure()
    lines = pyplot.plot(largs, profile)

    if title:
        pyplot.title(title)
    if x_label:
        pyplot.xlabel(x_label)
    if y_label:
        pyplot.ylabel(y_label)
    if extra_func:
        extra_func(pyplot, fig, lines)
        
    if legend_props:
        p = pyplot.subplot(1, 1, 1)
        box = p.get_position()
        p.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9]) # Shrink height by 20% for legend to go outside
        p.legend(prop=legend_props)

    utils.mk_path_for_file(graph_image)
    pyplot.savefig(graph_image)
    pyplot.close()


class GraphBase(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, **kwargs):
        '''
            legend: [('label', 'color'), ('label2', 'color2')]
        '''
        self.title = kwargs.get("title")
        self.x_label = kwargs.get("x_label")
        self.y_label = kwargs.get("y_label")
        self.legend = kwargs.get('legend')
        self.plot_methods = [self.plot]

    def decorations(self, ax):
        if self.title:
            ax.set_title(self.title)
        if self.x_label:
            ax.set_xlabel(self.x_label)
        if self.y_label:
            ax.set_ylabel(self.y_label)

    @abc.abstractmethod
    def plot(self, ax):
        return

    def post_plot(self, ax):
        if self.legend:
            patches = []
            labels = []
            for (name, color) in self.legend:
                labels.append(name)
                patches.append(Rectangle((0, 0), 1, 1, fc=color))

            ax.legend(patches, labels, loc='upper left')

    def figure(self, figure):
        ''' a hook method if you want to do something about the figure '''
        pass

    def get_figure_and_axis(self, dpi):
        figure = Figure(dpi=dpi)
        figure.patch.set_facecolor('white')
        ax = figure.add_subplot(1, 1, 1)
        return figure, ax

    def save(self, filename_or_obj, dpi=None, file_type='png'):
        figure, ax = self.get_figure_and_axis(dpi)
        
        self.decorations(ax)
        for plot in self.plot_methods:
            plot(ax) # Implementation
        self.post_plot(ax)
        self.figure(figure)

        if isinstance(filename_or_obj, basestring):
            utils.mk_path_for_file(filename_or_obj)
        
        canvas = FigureCanvasAgg(figure)
        if file_type == 'png':
            canvas.print_png(filename_or_obj)
        elif file_type == 'pdf':
            canvas.print_pdf(filename_or_obj)
        
    def draw_origin(self, ax):
        ax.axvline(c='black', lw=0.5) 
        ax.axhline(c='black', lw=0.5)

    def colorbar_from_cmap_array(self, figure, cmap, array):        
        mappable = ScalarMappable(cmap=cmap)
        mappable.set_array(array)
        figure.colorbar(mappable)



class CounterDistributionGraph(GraphBase):
    def __init__(self, counter, hide_once_below=None):
        ''' hide_once_below : Don't show outliers below this value '''
        super(CounterDistributionGraph, self).__init__()       
        self.counter = counter
        self.hide_once_below = hide_once_below
    
    def plot(self, ax):
        y = np.bincount(self.counter.values())

        highest_min = 0
        if self.hide_once_below:
            min_count = y > self.hide_once_below
            for (i, truth) in enumerate(min_count):
                if truth:
                    highest_min = i
    
            y = y[:highest_min+1]
            
        x = np.arange(len(y))
        ax.bar(x, y, width=1, color='black', edgecolor='none', bottom=0.001)
        ax.set_yscale('log')


