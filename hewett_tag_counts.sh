#!/bin/bash

# 1st parameter is FastQ to count
# Any parameters after that are used as existing barcode libraries to compare against. 

# NOTE: Cutadapt needs to be in path, you may need to run eg:
# module load cutadapt

FASTQ=$1
BARCODE_LIBRARY_FLAGS=
for (( i=2 ; i<=$# ; i++ )); do
	BARCODE_LIBRARY_FLAGS="${BARCODE_LIBRARY_FLAGS} --library ${!i}"
done

echo "BARCODE_LIBRARY_FLAGS=${BARCODE_LIBRARY_FLAGS}"


if [[ -z ${FASTQ} || ! -e ${FASTQ} ]]; then
	echo "Usage: $(basename $0) fastq" >&2;
	exit 1;
fi

CS1=ACAGGGACAGCAGAGATCCAGTTTGGTTAGTACCGGGCCC
CS2=GCGGCCGCTGAAAGACCCCACCTGTAGGTTTGGCAAGCTAGCTGCAGTAACG
BARCODE_PATTERN=ATNNTAANNATCNNGATSSAAANNGGTNNAACNNTGT
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SCRIPTS_DIR=${THIS_DIR}/scripts
export PYTHONPATH=${PYTHONPATH}:${THIS_DIR}


function trim_fastq_count_tags {
	READ=$1
	FRONT=$2
	BACK=$3
	BARCODE=$4
	
	NAME=$(basename ${READ} .fastq.gz)
	READ_TRIMMED=${NAME}.trimmed.fastq.gz
	LOG_FILE=${NAME}.cutadapt.log
	if [ ! -e ${READ_TRIMMED} ]; then
		cutadapt --trimmed-only --times=2 --front=${FRONT} --adapter=${BACK} ${READ} --output=${READ_TRIMMED} > $LOG_FILE
	else
		echo "Already trimmed"
	fi

	${SCRIPTS_DIR}/hewett_tag_counts.py --barcode=${BARCODE} ${BARCODE_LIBRARY_FLAGS} ${READ_TRIMMED}
}

function trim_sense_fastq {
	trim_fastq_count_tags $1 $CS1 $CS2 $BARCODE_PATTERN
}

function trim_antisense_fastq {
	FRONT=$(revcomp.py $CS2)
	BACK=$(revcomp.py $CS1)
	BARCODE=$(revcomp.py $BARCODE_PATTERN)
	trim_fastq_count_tags $1 $FRONT $BACK $BARCODE
}

if [[ $FASTQ =~ .*_R1_.*fastq.gz ]]; then
	trim_sense_fastq ${FASTQ}
elif [[ $FASTQ =~ .*_R2_.*fastq.gz ]]; then 
	trim_antisense_fastq ${FASTQ}
fi


