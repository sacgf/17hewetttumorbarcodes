Library Requirements
--------------------

Requires Python 2.7. Install libraries by running:

```
pip install -r requirements.txt
```

Running
-------

Main script is hewett_tag_counts.sh which:

 * Uses cutadapt to filter to sequences that have both adapters, and remove them
 * Run scripts/hewett_tag_counts.py on the trimmed files  

The 1st parameter is FastQ to count. Any other parameters after are used as existing barcode libraries to compare against.

Paired ends are treated separately, and things are automatically separated and reverse complimented etc depending on whether R1/R2 is in the FastQ file name.

Experiment
----------

The experiments for the paper involved running with just 1 FastQ from the initial runs: 

```
hewett_tag_counts.sh 140902DuncanHewett_MiSeq/Less_S1_L001_R1_001.fastq.gz
hewett_tag_counts.sh 140902DuncanHewett_MiSeq/Less_S1_L001_R2_001.fastq.gz
hewett_tag_counts.sh 140902DuncanHewett_MiSeq/More_S2_L001_R1_001.fastq.gz
hewett_tag_counts.sh 140902DuncanHewett_MiSeq/More_S2_L001_R2_001.fastq.gz
```

This produces initial library barcode files, which are then passed to all further scripts (keeping R1/R2 separate), eg:

```
hewett_tag_counts.sh 160311DuncanHewett_Miseq/fastq/S1_R1.fastq.gz 140902/Less_S1_L001_R1_001.trimmed.fastq.barcodes.txt 140902/More_S2_L001_R1_001.trimmed.fastq.barcodes.txt
```
